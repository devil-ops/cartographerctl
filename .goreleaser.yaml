---
version: 2
gitlab_urls:
  api: https://gitlab.oit.duke.edu/api/v4/
  download: https://gitlab.oit.duke.edu
  use_package_registry: true
  use_job_token: true
env:
  - VAULT_ADDR={{ .Env.VAULT_ADDR }}
  - TRANSIT_SECRET_ENGINE_PATH=ssi-systems/transit
  - VAULT_TOKEN={{ .Env.VAULT_TOKEN }}
builds:
  - env:
      - CGO_ENABLED=0
    main: ./cartographerctl
    mod_timestamp: '{{ .CommitTimestamp }}'
    flags:
      - -trimpath
    ldflags: >
      -s -w
      -X gitlab.oit.duke.edu/devil-ops/cartographerctl/cartographerctl/cmd.date={{ .CommitTimestamp }}
      -X gitlab.oit.duke.edu/devil-ops/cartographerctl/cartographerctl/cmd.version={{ .Tag }}
      -X gitlab.oit.duke.edu/devil-ops/cartographerctl/cartographerctl/cmd.commit={{ .ShortCommit }}'
    goos:
      - windows
      - linux
      - darwin
    goarch:
      - amd64
      - arm64
    ignore:
      - goos: windows
        goarch: arm6
    binary: 'cartographerctl'
archives:
  - format: tar.gz
    name_template: 'cartographerctl-{{ .Version }}_{{ .Os }}_{{ .Arch }}'
    format_overrides:
      - goos: windows
        format: zip
checksum:
  name_template: 'cartographerctl-{{ .Version }}_SHA256SUMS'
  algorithm: sha256
nfpms:
  - id: rpms
    bindir: /usr/bin
    license: MIT
    description: Interact with Cartographer from the CLI
    vendor: Duke University
    section: admin
    priority: optional
    homepage: https://gitlab.oit.duke.edu/devil-ops/
    maintainer: Drew Stinnett <drew.stinnett@duke.edu>
    rpm:
      signature:
        key_file: "{{ .Env.GPG_KEY_PATH }}"
    formats:
      - rpm
  - id: debs
    bindir: /usr/bin
    license: MIT
    description: Interact with Cartographer from the CLI
    vendor: Duke University
    section: admin
    priority: optional
    homepage: https://gitlab.oit.duke.edu/devil-ops/
    maintainer: Drew Stinnett <drew.stinnett@duke.edu>
    formats:
      - deb
  - id: apks
    bindir: /usr/bin
    license: MIT
    description: Interact with Cartographer from the CLI
    vendor: Duke University
    section: admin
    priority: optional
    homepage: https://gitlab.oit.duke.edu/devil-ops/
    maintainer: Drew Stinnett <drew.stinnett@duke.edu>
    formats:
      - apk
uploads:
  - name: productionrpm
    target: https://oneget.oit.duke.edu/rpm/devil-ops-rpms/
    method: PUT
    mode: archive
    username: api
    ids:
      - rpms
  - name: productiondeb
    target: https://oneget.oit.duke.edu/debian-packages/upload/devil-ops-debs/main/
    method: PUT
    mode: archive
    username: api
    ids:
      - debs
brews:
  - repository:
      owner: devil-ops
      name: homebrew-devil-ops
      token: "{{ .Env.HOMEBREW_TAP_TOKEN }}"
    description: "Cartographer CLI"
    homepage: "https://gitlab.oit.duke.edu/devil-ops/cartographerctl"
signs:
- cmd: cosign
  args: ["sign-blob", "--tlog-upload=false", "--key=hashivault://package-signing", "--output-signature=${signature}", "${artifact}"]
  artifacts: all
sboms:
  - artifacts: archive