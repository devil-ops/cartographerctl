# Cartographer CTL

To get started, just set these env vars:

```
export CARTOGRAPHER_USER=your_user
export CARTOGRAPHER_KEY=your_api_key
```

Or put them in your `~/.cartographerctl.yaml` file as:

```
---
user: your_user
key: your_api_key
```

## Use Cases

Generate a CSV containing some relevant subnet information

```
$ cartographerctl generate-csv subnet /tmp/subnets.csv
```

### Disclaimer

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
