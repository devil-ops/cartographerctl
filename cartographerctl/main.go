/*
Package main is the main executable
*/
package main

import "gitlab.oit.duke.edu/devil-ops/cartographerctl/cartographerctl/cmd"

func main() {
	cmd.Execute()
}
