/*
Package cmd holds the command line arguments
*/
package cmd

import (
	"cmp"
	"log/slog"
	"os"

	"github.com/spf13/cobra"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

var (
	cfgFile            string
	cartographerClient *cartographer.Client
	version            = "dev"
)

// Verbose Logging
var Verbose bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "cartographerctl",
	Short:   "Interact with Cartographer API",
	Long:    `Interact with Cartographer API`,
	Version: version,
	PersistentPreRun: func(_ *cobra.Command, _ []string) {
		// Initialize AKA client here
		cartographerClient = cartographer.NewClient(cartographer.ClientConfig{
			APIUser: viper.GetString("user"),
			APIKey:  viper.GetString("key"),
			APIURL:  cmp.Or(os.Getenv("CARTOGRAPHER_URL"), "https://cartographer.oit.duke.edu"),
		}, nil)

		// Are we talky?
		if Verbose {
			slog.SetDefault(
				slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
					Level: slog.LevelDebug,
				})))
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		slog.Error("fatal error", "error", err)
		os.Exit(2)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.cartographerctl.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			panic(err)
		}

		// Search config in home directory with name ".cartographerctl" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".cartographerctl")
		viper.SetEnvPrefix("cartographer")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		slog.Debug("Using config file", "file", viper.ConfigFileUsed())
	}
}
