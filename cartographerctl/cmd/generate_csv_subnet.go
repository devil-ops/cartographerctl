package cmd

import (
	"encoding/csv"
	"io"
	"log/slog"
	"os"
	"path"

	"github.com/spf13/cobra"
	"gitlab.oit.duke.edu/devil-ops/cartographer-sdk/cartographer"
)

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		slog.Error("error closing item", "error", err)
	}
}

// subnetCmd represents the subnet command
var subnetCmd = &cobra.Command{
	Use:   "subnet CSVFILE",
	Short: "Generate a CSV with Subnet Information",
	Args:  cobra.ExactArgs(1),
	Long: `Generate a CSV with useful subnet information. This currently includes:
VRF, CIDR and if the network is public or private`,
	RunE: func(_ *cobra.Command, args []string) error {
		outputFile := args[0]

		// Set up CSV File
		file, err := os.Create(path.Clean(outputFile))
		if err != nil {
			return err
		}
		defer dclose(file)

		writer := csv.NewWriter(file)
		defer writer.Flush()

		if err := writer.Write([]string{"VRF", "Subnet", "Public/Private"}); err != nil {
			return err
		}

		slog.Info("Looking up all addresses")
		vrfs, _, err := cartographerClient.VRF.List(nil)
		if err != nil {
			return err
		}
		for _, vrf := range vrfs {
			fullVRF, _, err := cartographerClient.VRF.Get(nil, vrf.Name)
			if err != nil {
				return err
			}
			for _, subnet := range fullVRF.Subnets {
				var networkType string
				if cartographer.IsPrivateIP(subnet.IP) {
					networkType = "private"
				} else {
					networkType = "public"
				}

				row := []string{vrf.Name, subnet.String(), networkType}
				if err := writer.Write(row); err != nil {
					return err
				}

			}
		}
		slog.Info("wrote file", "file", outputFile)
		return nil
	},
}

func init() {
	generateCsvCmd.AddCommand(subnetCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// subnetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// subnetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
